﻿namespace netChet
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonConect = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.RichTextBox();
            this.textBoxMesge = new System.Windows.Forms.TextBox();
            this.buttonMasge = new System.Windows.Forms.Button();
            this.textBoxIp = new System.Windows.Forms.TextBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // buttonConect
            // 
            this.buttonConect.Location = new System.Drawing.Point(168, 11);
            this.buttonConect.Name = "buttonConect";
            this.buttonConect.Size = new System.Drawing.Size(105, 23);
            this.buttonConect.TabIndex = 2;
            this.buttonConect.Text = "conect to server";
            this.buttonConect.UseVisualStyleBackColor = true;
            this.buttonConect.Click += new System.EventHandler(this.buttonConect_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(24, 50);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(248, 155);
            this.textBox2.TabIndex = 3;
            this.textBox2.Text = "";
            // 
            // textBoxMesge
            // 
            this.textBoxMesge.Location = new System.Drawing.Point(25, 250);
            this.textBoxMesge.Name = "textBoxMesge";
            this.textBoxMesge.Size = new System.Drawing.Size(247, 20);
            this.textBoxMesge.TabIndex = 4;
            // 
            // buttonMasge
            // 
            this.buttonMasge.Location = new System.Drawing.Point(59, 221);
            this.buttonMasge.Name = "buttonMasge";
            this.buttonMasge.Size = new System.Drawing.Size(176, 23);
            this.buttonMasge.TabIndex = 5;
            this.buttonMasge.Text = "send";
            this.buttonMasge.UseVisualStyleBackColor = true;
            this.buttonMasge.Click += new System.EventHandler(this.buttonMasge_Click);
            // 
            // textBoxIp
            // 
            this.textBoxIp.Location = new System.Drawing.Point(85, 11);
            this.textBoxIp.Name = "textBoxIp";
            this.textBoxIp.Size = new System.Drawing.Size(77, 20);
            this.textBoxIp.TabIndex = 6;
            this.textBoxIp.Text = "ip";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(2, 11);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(77, 20);
            this.textBoxName.TabIndex = 7;
            this.textBoxName.Text = "name";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 277);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.textBoxIp);
            this.Controls.Add(this.buttonMasge);
            this.Controls.Add(this.textBoxMesge);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.buttonConect);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonConect;
        private System.Windows.Forms.RichTextBox textBox2;
        private System.Windows.Forms.TextBox textBoxMesge;
        private System.Windows.Forms.Button buttonMasge;
        private System.Windows.Forms.TextBox textBoxIp;
        private System.Windows.Forms.TextBox textBoxName;
    }
}

