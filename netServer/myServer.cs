﻿using System;
using System.Threading;
using System.Net.Sockets;
using System.Text;
using System.Collections;

namespace myClass
{
    class myServer
    {
        public static int port = 8888;
        private static Hashtable clientsList = new Hashtable();
        public static void start()
        {
            TcpListener serverSocket = new TcpListener(port);
            TcpClient clientSocket = default(TcpClient);
            int counter = 0;

            serverSocket.Start();
            counter = 0;
            while ((true))
            {
                counter += 1;
                clientSocket = serverSocket.AcceptTcpClient();

                byte[] bytesFrom = new byte[byte.MaxValue];
                string dataFromClient = null;

                NetworkStream networkStream = clientSocket.GetStream();
                networkStream.Read(bytesFrom, 0, byte.MaxValue);
                dataFromClient = System.Text.Encoding.ASCII.GetString(bytesFrom);
                dataFromClient = dataFromClient.Substring(0, dataFromClient.IndexOf("$"));
                clientsList.Add(dataFromClient, clientSocket);
                broadcast(dataFromClient, "conect",new byte[0]);
                handleClinet client = new handleClinet();
                client.startClient(clientSocket, dataFromClient, clientsList);
            }

            clientSocket.Close();
            serverSocket.Stop();
        }
        public static mesg get = null;
        public static mesgByte getByte = null;
        public static void broadcast(string uName, string msg, byte[] msgByte)
        {
            if (get != null)
                get(uName, msg);
            if (getByte != null)
            {
                getByte(uName, msgByte);
            }
            foreach (DictionaryEntry Item in clientsList)
            {
                TcpClient broadcastSocket;
                broadcastSocket = (TcpClient)Item.Value;
                NetworkStream broadcastStream = broadcastSocket.GetStream();
                Byte[] broadcastBytes = null;
                broadcastBytes = Encoding.ASCII.GetBytes("[" + uName + "] " + msg);
                broadcastStream.Write(broadcastBytes, 0, broadcastBytes.Length);
                broadcastStream.Flush();
            }
        }
    }
    delegate void mesg(string name, string text);
    delegate void mesgByte(string name, byte[] text);
    public class handleClinet
    {
        TcpClient clientSocket;
        string clNo;
        Hashtable clientsList;

        public void startClient(TcpClient inClientSocket, string clineNo, Hashtable cList)
        {
            this.clientSocket = inClientSocket;
            this.clNo = clineNo;
            this.clientsList = cList;
            Thread ctThread = new Thread(doChat);
            ctThread.Start();
        }

        private void doChat()
        {
            int requestCount = 0;
            byte[] bytesFrom = new byte[10025];
            string dataFromClient = null;
            Byte[] sendBytes = null;
            string serverResponse = null;
            string rCount = null;
            requestCount = 0;

            while ((true))
            {
                try
                {
                    requestCount = requestCount + 1;
                    NetworkStream networkStream = clientSocket.GetStream();
                    networkStream.Read(bytesFrom, 0, 10024);
                    dataFromClient = System.Text.Encoding.ASCII.GetString(bytesFrom);
                    dataFromClient = dataFromClient.Substring(0, dataFromClient.IndexOf("$"));
                    if (dataFromClient == "exit")
                    {
                        myServer.broadcast(clNo, "remove", new byte[0]);
                        clientsList.Remove(clNo);
                        return;
                    }
                    Byte[] g = new Byte[dataFromClient.Length];
                    Array.Copy(bytesFrom, g, dataFromClient.Length);
                    myServer.broadcast(clNo, dataFromClient, g);
                    rCount = Convert.ToString(requestCount);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }
    }
}